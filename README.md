Se ejecuta la siguiente instrucción:

~~~~
make jar
~~~~

## Uso del catalogo de coches:

Permite ejecutar las instrucciones que se muestran a continuación: 
     
1. Mostrar coches:

~~~~
 java -jar catalogo.jar show
~~~~

2. Mostrar ayuda: 

~~~~
java -jar catalogo.jar help
~~~~

3. Añadir coche:

~~~~
java -jar catalogo.jar add <nombre> <modelo>
~~~~

    por ejemplo,
~~~~
java -jar catalogo.jar add Citroen C5
~~~~

# Notas para los desarrolladores

## Generación de Javadoc

Se ejecuta la siguiete instrucción:

~~~~
make javadoc
~~~~ 

## Inspección de Javadoc

Suponiendo que tiene instalado `firefox`, se ejecuta:

~~~~
firefox htlm/index.html
~~~~

## Sobre el fichero _makefile_

Se han utilizado sentencias específicas de Linux, por tanto, sólo
ejecuta en este sistema operativo.

