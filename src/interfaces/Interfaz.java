package interfaces;

import dominio.Catalogo;
import dominio.Coche;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
	private static Catalogo catalogo = new Catalogo();
	private static void mostrarAyuda(){
		System.out.println("Las instrucciones posibles son las siguientes:");
		System.out.println(" 1. Mostrar coches: java -jar catalogo.jar show");
		System.out.println(" 2. Mostrar esta ayuda: java -jar catalogo.jar help");
		System.out.println(" 3. Añadir coche:  java -jar catalogo.jar add <Nombre> <Modelo> ");
		System.out.println(" java -jar catalogo.jar add Citroen c5 ");
	}
	public static void ejecutar(String[] args){
		try 
		{
			if (args[0].equalsIgnoreCase("add"))
				catalogo.annadirCoche(new Coche(args[1], args[2]));
			else if (args[0].equalsIgnoreCase("show"))
				catalogo.mostrarCoches();
			else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
			else mostrarAyuda();
		}catch(ArrayIndexOutOfBoundsException ex){
			mostrarAyuda();
		}
	}
}
			
