package dominio;

public class Coche {
	private String nombre;
	private String modelo;

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Coche(String nombre, String modelo) {
		this.nombre = nombre;
		this.modelo = modelo;
	}
	@Override
	public String toString() {
		return getNombre() + " " + getModelo() + "\n";
	}


}
