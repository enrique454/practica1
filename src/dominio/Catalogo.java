package dominio;

import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileWriter;

public class Catalogo{
	private ArrayList<Coche> listaCoches = new ArrayList<>();
	private static String nombreFichero = "Catalogo.txt";

	public Catalogo(){
		cargarDesdeFichero();
	}
	public void annadirCoche(Coche coche){
		listaCoches.add(coche);
		volcarAFichero();
	}
	public void mostrarCoches()
	{
		for(Coche coche : listaCoches) System.out.println(coche);
	}
	private void volcarAFichero(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.err.println("Error al intentar escribir en fichero");
		}
	}
	private void cargarDesdeFichero(){
		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()) {
				System.out.println("Acaba de crearse un nuevo fichero");
			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					listaCoches.add(new Coche(sc.next(), sc.next()));
				}
			}
		}catch(IOException ex){
		}
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Coche coche : listaCoches) sb.append(coche + "\n");
		return sb.toString();
	}
}
		

